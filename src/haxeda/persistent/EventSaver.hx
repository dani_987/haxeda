package haxeda.persistent;

import haxe.Json;
import haxeda.events.UpdateData;

typedef KeyValue = Map<String,Dynamic>;

abstract class EventSaver {
    private var globalData : KeyValue = new Map();
    private var keyValData : Map<String,KeyValue> = new Map();
    private var history : Array<EventHistory> = [];
    private var wasLoaded : Bool = false;

    private function new(){}

    abstract private function triggerSave() : Void;

    abstract private function triggerLoad() : Void;

	public function save(incomingTime:Float, eventName:String, changes:List<UpdateData>) {
        var dataBefore : List<UpdateData> = new List();
        var deletedMapKeys : List<String> = new List();
        for(change in changes){
            switch(change.getChange()){
                case GlobalDataChange(name, type, _):
                    dataBefore.add(UpdateData.fromUpdateDataChange(GlobalDataChange(
                        name, type, globalData[Json.stringify({n:name,t:type})]
                    )));
                case KeyValueDataChange(key,keyval,name,type,_):
                    var mapKey = Json.stringify({k:key,kv:keyval});
                    var dataMap = keyValData[mapKey];
                    if(dataMap == null){
                        if(deletedMapKeys.filter(str -> str == mapKey).isEmpty()){
                            deletedMapKeys.add(mapKey);
                            dataBefore.add(UpdateData.fromUpdateDataChange(DeleteKeyChange(
                                key,keyval
                            )));
                        }
                    } else {
                        dataBefore.add(UpdateData.fromUpdateDataChange(KeyValueDataChange(
                            key,keyval,name,type,dataMap[Json.stringify({n:name,t:type})]
                        )));
                    }
                case DeleteKeyChange(key, keyval):
                    var kvData = keyValData[Json.stringify({k:key,kv:keyval})];
                    if(kvData != null){
                        for(json => data in kvData){
                            var meta : {n:String,t:String} = Json.parse(json);
                            dataBefore.add(UpdateData.fromUpdateDataChange(KeyValueDataChange(
                                key,keyval,meta.n,meta.t,data
                            )));
                        }
                    }
            }
        }
        history.push({
            incomingTime:incomingTime,
            event:eventName,
            dataBefore:[for(before in dataBefore) before],
            dataAfter:[for(change in changes) change],
        });
        for(change in changes){
            switch(change.getChange()){
                case GlobalDataChange(name, type, data):
                    globalData[Json.stringify({n:name,t:type})] = data;
                case KeyValueDataChange(key,keyval,name,type,data):
                    var dataKey = Json.stringify({k:key,kv:keyval});
                    var keyVal = keyValData[dataKey];
                    if(keyVal == null){
                        keyVal = new Map();
                    }
                    keyVal[Json.stringify({n:name,t:type})] = data;
                    keyValData[dataKey] = keyVal;
                case DeleteKeyChange(key, keyval):
                    keyValData.remove(Json.stringify({k:key,kv:keyval}));
            }
        }
        triggerSave();
    }

    private function loadIfRequired() {
        if(!wasLoaded){
            wasLoaded = true;
            triggerLoad();
        }
    }

	public function loadHistory():Array<EventHistory> {
        loadIfRequired();
		return history;
	}

	public function loadData(): List<UpdateData> {
        loadIfRequired();
        var result : List<UpdateData> = new List();
        for(json => data in globalData){
            var meta : {n:String,t:String} = Json.parse(json);
            result.add(UpdateData.fromUpdateDataChange(GlobalDataChange(
                meta.n, meta.t, data
            )));
        }
        for(jsonK => keyVal in keyValData){
            for(json => data in keyVal){
                var metaKeyVal : {k:String,kv:String} = Json.parse(jsonK);
                var meta : {n:String,t:String} = Json.parse(json);
                result.add(UpdateData.fromUpdateDataChange(KeyValueDataChange(
                    metaKeyVal.k, metaKeyVal.kv, meta.n, meta.t, data
                )));
            }
        }
        return result;
	}

}
