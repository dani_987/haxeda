package haxeda.persistent;

import haxeda.events.UpdateData;

typedef EventHistory = {
    incomingTime:Float,
    event:String,
    dataBefore:Array<UpdateData>,
    dataAfter:Array<UpdateData>,
};
