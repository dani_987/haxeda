package haxeda.persistent;

import hx.io.converter.format.HaxeExtendedDynamicFormat;
import haxe.ds.List;
import hx.streams.ErrorDetail.ErrorDetailFollower;
import hx.streams.StringInput;
import haxe.io.Bytes;
import hx.streams.FileInput;
import hx.io.converter.format.HaxeBinFormat;
import hx.io.converter.FileAccessor;
import hx.streams.StringOutput;

using hx.io.converter.DynamicStructureTools;

class DefaultEventSaver extends EventSaver {
    private var path: String;
    private var saveHistory: Bool;

    public function new(path: String, saveHistory: Bool = true) {
        super();
        this.path = path;
        this.saveHistory = saveHistory;
    }

	function triggerSave() {
        var objToSave = {
            global:globalData,
            history:history,
            keyVal:keyValData,
        };
        if(!saveHistory){
            objToSave.history=[];
        }
        var output = new StringOutput(path);
        switch(HaxeExtendedDynamicFormat.writeToOutput(output, objToSave)){
            case Some(errorMessage):
                trace(errorMessage);
            case None:
                #if(js)
                js.Browser.window.localStorage.setItem(path, output.getBytes().toHex());
                #else
                sys.io.File.saveBytes(path, output.getBytes());
                #end

        }
    }

	function triggerLoad() {
        #if(js)
        var data = js.Browser.window.localStorage.getItem(path);
        if(data == null || data.length == 0)return;
        var input = new StringInput(Bytes.ofHex(data) ,'localStorage:$path');
        #else
        if(!sys.FileSystem.exists(path) || sys.FileSystem.isDirectory(path))return;
        var input = new FileInput(path);
        #end
        switch(HaxeExtendedDynamicFormat.parseFromInput(input)){
            case Success(decodedAny):
                var decoded = cast(decodedAny);
                this.globalData = decoded.global;
                this.history = decoded.history;
                this.keyValData = decoded.keyVal;
            case WithError(error):
                ErrorDetailFollower.trace(error);
        }
    }
}
