package haxeda.events;

interface EventIdCreator {
    public function getNextId(): Int;
}
