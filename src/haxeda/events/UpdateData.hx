package haxeda.events;

using haxe.EnumTools.EnumValueTools;

enum UpdateDataChange {
    GlobalDataChange(name: String, type: String, data: Dynamic);
    KeyValueDataChange(key: String, keyval: String, name: String, type: String, data: Dynamic);
    DeleteKeyChange(key: String, keyval: String);
}
class UpdateData {
    private var key : String;
    private var keyval : String;
    private var name : String;
    private var type : String;
    private var data : Dynamic;
    private var isDelete : Bool;

    public function getChange(): UpdateDataChange {
        if(key == ""){
            return GlobalDataChange(name, type, data);
        } else if(isDelete) {
            return DeleteKeyChange(key, keyval);
        } else {
            return KeyValueDataChange(key, keyval, name, type, data);
        }
    }

    public function toString(): String {
        var changeEnum = this.getChange();
        var parameters = changeEnum.getParameters();
        var parString = if(parameters.length == 0) "" else '(${parameters.join(",")})';
        var enumName = changeEnum.getName();
        return enumName+parString;
    }

    public static function fromUpdateDataChange(from: UpdateDataChange) : UpdateData{
        switch(from){
            case GlobalDataChange(name, type, data):
                return new UpdateData("", "", name, type, data, false);
            case KeyValueDataChange(key, keyval, name, type, data):
                return new UpdateData(key, keyval, name, type, data, false);
            case DeleteKeyChange(key, keyval):
                return new UpdateData(key, keyval, "", "", null, true);
        }
    }

    public function new(key: String, keyval: String, name: String, type: String, data : Dynamic, isDelete: Bool){
        this.key = key;
        this.keyval = keyval;
        this.name = name;
        this.type = type;
        this.data = data;
        this.isDelete = isDelete;
    }

    public function trace() {
        trace(this.getChange());
    }

    public function updateValIfMatches(name: String, type: String, setter: Dynamic->Void){
        if( (this.key == "")
         && (this.name == name)
         && (this.type == type)
         && (!this.isDelete)
        ){
            setter(this.data);
        }
    }

    public function updateMapValIfMatches(key: String, name: String, type: String, setter: (String,Dynamic)->Void){
        if( (this.key == key)
         && (this.name == name)
         && (this.type == type)
         && (!this.isDelete)
        ){
            setter(this.keyval, this.data);
        }
    }

    public function deleteKeyIfMatches(key: String, deleter: (String)->Void){
        if( (this.key == key)
         && (this.isDelete)
        ){
            deleter(this.keyval);
        }
    }
}
