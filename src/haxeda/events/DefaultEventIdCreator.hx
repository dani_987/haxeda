package haxeda.events;

class DefaultEventIdCreator implements EventIdCreator {
    private var lastId = 0;
    public function new() {}

    public function getNextId() : Int {
        lastId++;
        return lastId;
    }
}
