package haxeda;

abstract class EventEngineSuperviser {
    public function onListenerAdded(listener: EventListener){}
    public function onListenerFunctionAdded(eventName: String, toListener: EventListener){}

    public function onListenerRemoved(listener: EventListener) {}
    public function onListenerFunctionRemoved(eventName: String, toListener: EventListener) {}

    public function onEvent(eventName: String, event: Event){}
    public function onEventTriggered(eventName: String, event: Event, listener: EventListener) {}
}
