package haxeda;


enum EventProcessingResult{
    Success();
    WithError(message: String);
    StopProcessing(retryInMs: Int);
}

@:autoBuild(haxeda.HaxedaTypeBuilder.buildEventListener())
interface EventListener {
    public function __update(dataUpdates: List<haxeda.events.UpdateData>) : Void;
}
