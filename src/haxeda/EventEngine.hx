package haxeda;
import haxeda.events.DefaultEventIdCreator;
import haxeda.events.EventIdCreator;
import haxeda.persistent.EventSaver;
import haxeda.events.ApplicationExit;
import haxeda.events.UpdateData;
import haxe.Timer;
using haxeda.Event;
using haxeda.EventListener;
using haxeda.locks.Lock;

enum PipelineStatus{
    Active;
    Stopped;
    Waiting(forMs:Int, timeAtStart: Float);
}

enum EventEngineMode{
    RunTilKillEvent;
    RunTilNoneWaiting;
    RunTilReturns;
}

class EventEngine {
    private var listeners : List<EventListener>;
    private var eventPipeline : Map<EventListener,{addingTime: Int, accessToken:Lock,status:PipelineStatus, updates: List<{time: Int, type: String, data: List<UpdateData>}>,funs: Map<String,{fun: Event->EventProcessingResult, pipeline:List<Event>}>}>;

    private var processToken = new Lock();
    private var pipelineAccessToken = new Lock();
    private var listenersAccessToken = new Lock();

    private var pipelineItmes = 0;

    private var isProcessingFun : Null<String>;

    private var isProcessing : Bool;

    private var nextUpdateTime : Float;

    private var gotKillEvent : Bool;
    private var killEventAdded : Int;

    private var saver: Null<EventSaver>;
    private var supervisors: Array<EventEngineSuperviser>;

    private var idCreator: EventIdCreator;
    private var listenerIdCreator : EventIdCreator = new DefaultEventIdCreator();

    public function new(
        saver: Null<EventSaver> = null,
        supervisors: Array<EventEngineSuperviser> = null,
        idCreator: EventIdCreator = null,
    ){
        listeners = new List<EventListener>();
        eventPipeline = new Map();
        isProcessing = false;
        isProcessingFun = null;
        gotKillEvent = false;
        nextUpdateTime = 0;
        killEventAdded = 0;
        this.saver = saver;
        this.supervisors = if(supervisors == null) [] else supervisors;
        this.idCreator = if(idCreator == null) new DefaultEventIdCreator() else idCreator;
    }

    @:generic
    public function registerFunction<E:Event>(toListenOn: Class<E>, isPartOf: EventListener, fun: E->EventProcessingResult) : Bool{
        var eventName = Classes.getFullName(toListenOn);
        for(supervisor in supervisors)supervisor.onListenerFunctionAdded(eventName,isPartOf);
        pipelineAccessToken.acquire();
        var pipeline = eventPipeline.get(isPartOf);
        if(pipeline == null){
            listeners.add(isPartOf);
            pipeline = {addingTime: listenerIdCreator.getNextId(), accessToken: new Lock(), status:Active, updates: new List(),funs: new Map()};
            eventPipeline.set(isPartOf, pipeline);
        }
        if(pipeline.funs.get(eventName) == null){
            pipeline.accessToken.acquire();
            pipeline.funs.set(eventName, {fun:cast fun, pipeline: new List()});
            pipeline.accessToken.release();
            pipelineAccessToken.release();
            return true;
        }
        pipelineAccessToken.release();
        return false;
    }

    private function keepCountersUpToDateAtRemove(
        toRemove: {pipeline:List<Event>},
        eventName: String
    ){
        if(isProcessingFun == eventName){
            pipelineItmes++;
        } else {
            for(event in toRemove.pipeline){
                if (this.checkForKillEvent(event, false)){
                    this.killEventAdded--;
                    if(this.killEventAdded == 0){
                        this.gotKillEvent = true;
                    }
                }
            }
        }
        pipelineItmes-=toRemove.pipeline.length;
    }

    public function arePipelinesEmpty() : Bool {
        return pipelineItmes <= 0;
    }
    public function wasApplicationExitSend() : Bool {
        return gotKillEvent;
    }

    @:generic
    public function removeFunction<E:Event>(toListenOn: Class<E>, isPartOf: EventListener, fun: E->EventProcessingResult) : Bool {
        var eventName = Classes.getFullName(toListenOn);
        for(supervisor in supervisors)supervisor.onListenerFunctionRemoved(eventName,isPartOf);
        pipelineAccessToken.acquire();
        var pipeline = eventPipeline.get(isPartOf);
        if(pipeline != null){
            pipeline.accessToken.acquire();
            var fun = pipeline.funs[eventName];
            if(fun != null){
                keepCountersUpToDateAtRemove(fun, eventName);
                pipeline.funs.remove(eventName);
            }
            pipeline.accessToken.release();
            pipelineAccessToken.release();
            return true;
        }
        pipelineAccessToken.release();
        return false;
    }

    public function registerListener(listenerClass: EventListener){
        for(supervisor in supervisors)supervisor.onListenerAdded(listenerClass);
        pipelineAccessToken.acquire();
        var pipeline = eventPipeline.get(listenerClass);
        if(pipeline == null){
            listeners.add(listenerClass);
            pipeline = {addingTime: listenerIdCreator.getNextId(), accessToken: new Lock(), status:Active, updates: new List(),funs: new Map()};
            eventPipeline.set(listenerClass, pipeline);
        }
        pipelineAccessToken.release();
    }

    public function removeListener(listenerClass: EventListener){
        for(supervisor in supervisors)supervisor.onListenerRemoved(listenerClass);
        pipelineAccessToken.acquire();
        var pipeline = eventPipeline[listenerClass];
        if(pipeline != null)
        {
            var token = pipeline.accessToken;
            token.acquire();
            for(eventName => fun in pipeline.funs){
                keepCountersUpToDateAtRemove(fun, eventName);
            }
            eventPipeline.remove(listenerClass);
            token.release();
        }
        pipelineAccessToken.release();
    }

    private function checkForKillEvent(toTrigger:Event, killEventBefore: Bool) : Bool {
        if( Std.isOfType(toTrigger, ApplicationExit) ){
            return true;
        }
        return killEventBefore;
    }

    public function triggerEvent(toTrigger:Event){
        saveEvent(toTrigger);
        addEventToPipelines(toTrigger);
        processPipelines();
    }

    private function addEventToPipelines(toTrigger:Event, ?pos:haxe.PosInfos){
        if(null == toTrigger)throw 'addEventToPipelines cannot be called on null - ${pos.fileName}:${pos.lineNumber}!';
        var added = 0;
        var eventName = Classes.getFullName(Classes.fromInstance(toTrigger));
        var updateData = toTrigger.__getUpdateData();

        for(supervisor in supervisors)supervisor.onEvent(eventName, toTrigger);

        pipelineAccessToken.acquire();
        var eventTime = idCreator.getNextId();
        for(pipeline in eventPipeline){
            pipeline.accessToken.acquire();
            pipeline.updates.add({time: eventTime, type: eventName, data: updateData});
            switch(pipeline.funs.get(eventName)){
                case null:
                case {pipeline:list}:
                    pipelineItmes++;
                    added++;
                    list.add(toTrigger);
            }
            pipeline.accessToken.release();
        }
        if( 0 == added){
            this.gotKillEvent = this.checkForKillEvent(toTrigger, this.gotKillEvent);
        } else {
            if(this.checkForKillEvent(toTrigger, false)){
                this.killEventAdded = added;
            }
        }
        pipelineAccessToken.release();
    }

    private function setNextUpdateTime(waitForMs: Int, startTime: Float) {
        var nextUpdateTimeThis = startTime + ( (waitForMs + 5)/ 1000);
        if(nextUpdateTime < Timer.stamp()){
            nextUpdateTime = nextUpdateTimeThis;
        } else {
            nextUpdateTime = Math.min(nextUpdateTime, nextUpdateTimeThis);
        }
    }

    private function getNextEventListenerToProcess() : Null<EventListener> {
        var result : Null<EventListener> = null;
        pipelineAccessToken.acquire();
        var orderedPipeline = [
            for(listener => events in eventPipeline){listener:listener,events:events}
        ];
        orderedPipeline.sort((a,b) -> {//a < b -> -1
            if(b.events.updates.isEmpty()){
                if(a.events.updates.isEmpty())return 0;
                return -1;
            }
            if(a.events.updates.isEmpty())return 1;

            var cmp_a = a.events.updates.first().time;
            var cmp_b = a.events.updates.first().time;

            if (cmp_a == cmp_b){
                return Reflect.compare(a.events.addingTime, b.events.addingTime);
            }
            else return Reflect.compare(cmp_a, cmp_b);
        });
        for(possibleNext in orderedPipeline){
            possibleNext.events.accessToken.acquire();
            if(!possibleNext.events.updates.isEmpty()){
                switch(possibleNext.events){
                    case {status:Active}:
                        result = possibleNext.listener;
                    case {status:Waiting(forMs,timeAtStart)}:
                        if((Timer.stamp() - timeAtStart) * 1000 >= forMs){
                            result = possibleNext.listener;
                        } else {
                            setNextUpdateTime(forMs,timeAtStart);
                        }
                    default:
                }
            }
            possibleNext.events.accessToken.release();
            if(result != null)break;
        }
        pipelineAccessToken.release();
        return result;
    }

    public function processPipelines(){
        if(!processToken.tryAcquire())return;
        if(isProcessing){
            processToken.release();
            return;
        }
        isProcessing=true;

        var killEvent = this.gotKillEvent;
        var listener = getNextEventListenerToProcess();
        while(listener != null){
            pipelineAccessToken.acquire();
            var pipelineData = eventPipeline.get(listener);
            pipelineData.accessToken.acquire();
            var updateData = pipelineData.updates.first();
            if(updateData == null){
                pipelineData.accessToken.release();
                pipelineAccessToken.release();
                listener = getNextEventListenerToProcess();
                continue;
            }
            var fun = pipelineData.funs.get(updateData.type);
            if(fun != null){
                isProcessingFun = updateData.type;
                var processingEvent = fun.pipeline.first();
                var processingResult = Success;
                if(processingEvent != null)
                {
                    killEvent = checkForKillEvent(processingEvent, killEvent);
                    for(supervisor in supervisors){
                        supervisor.onEventTriggered(
                            Classes.getFullName(Classes.fromInstance(processingEvent)),
                            processingEvent,
                            listener
                        );
                    }
                    processingResult = fun.fun(processingEvent);
                    isProcessingFun = null;
                } else {
                    trace(fun);
                }
                switch(processingResult){
                    case Success:
                        pipelineItmes--;
                        fun.pipeline.pop();
                        listener.__update(cast updateData.data);
                        pipelineData.updates.pop();
                    case WithError(error):
                        trace('${Classes.getSimpleName(Classes.fromInstance(listener))} returned Error "$error" for Message ${updateData.type}.');
                        pipelineData.status = Stopped;
                    case StopProcessing(retryInMs):
                        var now = Timer.stamp();
                        pipelineData.status = Waiting(retryInMs, now);
                        setNextUpdateTime(retryInMs, now);
                }
            } else {
                listener.__update(cast updateData.data);
                pipelineData.updates.pop();
            }
            pipelineData.accessToken.release();
            pipelineAccessToken.release();
            listener = getNextEventListenerToProcess();
        }
        isProcessing=false;
        this.gotKillEvent = killEvent;
        processToken.release();
    }

    private function saveEvent(toSave:Event) {
        if(saver == null){
            return;
        }
        saver.save(
            Timer.stamp(),
            Type.getClassName(Type.getClass(toSave)),
            toSave.__getUpdateData()
        );
    }

    private function loadSavedData() {
        if(saver == null){
            return;
        }
        processToken.acquire();
        pipelineAccessToken.acquire();

        var dataList = saver.loadData();

        for(listener => _ in eventPipeline){
            listener.__update(dataList);
        }
        pipelineAccessToken.release();
        processToken.release();
    }

    public function start(mode: EventEngineMode) {
        this.loadSavedData();
        this.triggerEvent(new haxeda.events.ApplicationStarted());
        this.keepRunning(mode);
    }

    public function keepRunning(mode: EventEngineMode) {
        do{
            this.processPipelines();
        }while( ! isExecutionFinished(mode));
    }

    private function sleepForSec(timeToSleepSec: Float){
        #if(sys)
            Sys.sleep( timeToSleepSec );
        #else
            //No sleep possible, so toggle a Bool until the time ends...
            var timeEnd = haxe.Timer.stamp() + (timeToSleepSec);
            var nop : Bool = false;
            while(timeEnd > haxe.Timer.stamp()){
                nop = !nop;
            }
        #end
    }

    private function isExecutionFinished(mode: EventEngineMode) : Bool {
        if(mode == RunTilReturns){
            return true;
        }
        var now = Timer.stamp();
        if( pipelineItmes > 0 ){
            if(nextUpdateTime > now){
                sleepForSec(nextUpdateTime - now);
                return false;
            }
        }
        if (mode == RunTilKillEvent) {
            #if(sys)
                Sys.sleep( 0.2 );
            #end
            return gotKillEvent;
        } else {
            return true;
        }
    }
}
