package haxeda;

class Classes {
    public static inline function getSimpleName(fromClass: Class<Any>) : String {
        return ~/^.*\./g.replace(getFullName(fromClass),"");
    }
    public static inline function getFullName(fromClass: Class<Any>) : String {
        return Type.getClassName(fromClass);
    }

    public static inline function fromInstance(instance: Any) : Class<Any> {
        return Type.getClass(instance);
    }
}
