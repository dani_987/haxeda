package haxeda;

@:autoBuild(haxeda.HaxedaTypeBuilder.buildEvent())
interface Event {
    public function __getUpdateData() : List<haxeda.events.UpdateData>;
}
