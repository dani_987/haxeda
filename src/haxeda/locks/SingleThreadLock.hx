package haxeda.locks;

// In single Threaded we don't need to do anything here
class SingleThreadLock {
    inline public function new() {}

    inline public function acquire() {}

    inline public function tryAcquire() : Bool {return true;}

    inline public function release() {}
}
