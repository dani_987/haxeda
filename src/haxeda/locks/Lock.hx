package haxeda.locks;

class Lock {
    #if (target.threaded)
        final lock = new sys.thread.Mutex();
    #else
        // single threaded targets like js, lua, php
        final lock = new SingleThreadLock();
    #end

    inline public function new() {}

    inline public function acquire() {
        lock.acquire();
    }

    inline public function tryAcquire() : Bool {
        return lock.tryAcquire();
    }

    inline public function release() {
        lock.release();
    }
}
