package haxeda;

interface EventEngineRegisterer {
    public function registerToEventEngine(eventEngineToRegister: EventEngine): Bool;
}
