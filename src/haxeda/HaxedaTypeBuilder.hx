package haxeda;

import haxe.macro.ComplexTypeTools;
using haxe.macro.Type;
using haxe.macro.Expr;
using haxe.macro.Expr.ComplexType;
using haxe.macro.Expr.Field;
import haxe.macro.Context;
import haxe.macro.Expr.TypeParam;
import haxe.macro.Expr.ObjectField;


typedef SimpleField = {name:String,pos:Position, initValue: Expr};
typedef TypedField = {> SimpleField, type:String};
typedef FullField = {> SimpleField, subfields: Array<TypedField>, type: ComplexType, isStruct: Bool};

typedef MetaData = {keyName: String, dependentOnKey: Bool, keyDef: Position}

class HaxedaTypeBuilder {
    static private function getStructInit(pos:Position, fields:Array<SimpleField>, init: Expr) : Expr {
        if(init != null)return init;
        var defs:Array<ObjectField> = new Array<ObjectField>();
        for(field in fields){
            defs.push({
                field:field.name,
                expr: {pos: field.pos, expr: field.initValue.expr},
                quotes: Unquoted,
            });
        }
        return {expr:EObjectDecl(defs),pos:pos};
    }

    static private function getInitValue(optExpr:Null<Expr>) : Expr {
        if(optExpr != null)return optExpr;
        return macro null;
    }

    static private function getRawField(fromField: Field) : Null<{> TypedField, complexType: ComplexType}> {
        switch(fromField.kind){
            case FVar(ctype=TPath(path),optExpr):
                switch(path.name){
                    case "String" | "Int" | "Bool" | "Float":
                        return {
                            name: fromField.name,
                            pos: fromField.pos,
                            type: path.name,
                            initValue: getInitValue(optExpr),
                            complexType: ctype,
                        };
                    default:
                }
            default:
        }
        return null;
    }

    static private function toTypedField(f: {name: String, pos: Position, type: Type}) : Null<TypedField> {
        switch(f.type){
            case TInst(_.get().name => tname,_) | TAbstract(_.get().name => tname,_):
                return {
                    name: f.name,
                    pos: f.pos,
                    type: tname,
                    initValue: macro null,
                };
            default: return null;
        }
    }

    static private function toTyped(f: Field) : Null<TypedField> {
        switch(f.kind){
            case FVar(TPath(path),_):
                return {
                    name: f.name,
                    pos: f.pos,
                    type: path.name,
                    initValue: macro null,
                };
            default: return null;
        }
    }



    static private function toFullField(fromField: Field) : Null<FullField> {
        var resultField : Null<FullField> = null;
        var rawField = getRawField(fromField);
        if(rawField != null){
            resultField = {
                name: rawField.name,
                pos: rawField.pos,
                type: rawField.complexType,
                initValue: rawField.initValue,
                subfields: [rawField],
                isStruct: false,
            };
        }else{
            switch(fromField.kind){
                case FVar(tpath=TPath(_),exprRef):
                    switch(ComplexTypeTools.toType(tpath)){
                        case TType(_.get().type => TAnonymous(anonymus), _):
                            var subfields = new Array<TypedField>();
                            for(f in anonymus.get().fields){
                                var typed = toTypedField(f);
                                if(typed != null){subfields.push(typed);}
                            }
                            resultField = {
                                name: fromField.name,
                                pos: fromField.pos,
                                type: tpath,
                                initValue: getStructInit(fromField.pos, subfields, exprRef),
                                subfields: subfields,
                                isStruct: true,
                            };
                        case TEnum(_.get() => enumType, params):
                            resultField = {
                                name: fromField.name,
                                pos: fromField.pos,
                                type: tpath,
                                initValue: exprRef,
                                subfields: [{
                                    name: fromField.name,
                                    pos: fromField.pos,
                                    type: enumType.name,
                                    initValue: exprRef
                                }],
                                isStruct: false,
                            };
                        case TType(_.get() => type, _):
                            resultField = {
                                name: fromField.name,
                                pos: fromField.pos,
                                type: tpath,
                                initValue: exprRef,
                                subfields: [{
                                    name: fromField.name,
                                    pos: fromField.pos,
                                    type: type.name,
                                    initValue: exprRef
                                }],
                                isStruct: false,
                            };
                        default:
                    }
                case FVar(tanonym=TAnonymous(structFields), exprRef):
                    var subfields = new Array<TypedField>();
                    for(f in structFields){
                        var typed = toTyped(f);
                        if(typed != null){subfields.push(typed);}
                    }
                    resultField = {
                        name: fromField.name,
                        pos: fromField.pos,
                        type: tanonym,
                        initValue: getStructInit(fromField.pos, subfields, exprRef),
                        subfields: subfields,
                        isStruct: true,
                    };
                case FVar(textend=TExtend(extendedList,structFields),exprRef):
                    var subfields = new Array<TypedField>();
                    for(extended in extendedList){
                        switch(ComplexTypeTools.toType(ComplexType.TPath(extended))){
                            case TType(_.get().type => TAnonymous(anonymus), _):
                                for(f in anonymus.get().fields){
                                    var typed = toTypedField(f);
                                    if(typed != null){subfields.push(typed);}
                                }
                            default:
                        }
                    }
                    for(f in structFields){
                        var typed = toTyped(f);
                        if(typed != null){subfields.push(typed);}
                    }
                    resultField = {
                        name: fromField.name,
                        pos: fromField.pos,
                        type: textend,
                        initValue: getStructInit(fromField.pos, subfields, exprRef),
                        subfields: subfields,
                        isStruct: true,
                    };
                default:
            }
        }
        return resultField;
    }

    static private function getUpdateableFieldData(fromField: Field) : Null<{meta: MetaData, field: FullField}> {
        var metaData = {keyName: "", dependentOnKey: false, keyDef: fromField.pos};
        var hasToBeUpdated = false;
        for(meta in fromField.meta){
            switch(meta.name) {
                case "update":
                    hasToBeUpdated = true;
                    switch(meta.params){
                        case []:
                        default: Context.fatalError("@update has no Parameters!", meta.pos);
                    }
                case "keyTable":
                    hasToBeUpdated = true;
                    switch(meta.params){
                        case [{expr:EConst(CString(name,_))}]:
                            metaData.keyName = name;
                            metaData.dependentOnKey = true;
                            metaData.keyDef = meta.pos;
                        default: Context.fatalError("@keyTable has 1 required String Parameter", meta.pos);
                    }
                default: continue;
            }
        }
        if( ! hasToBeUpdated ){
            return null;
        }
        switch(toFullField(fromField)){
            case null: return null;
            case field: return {
                meta: metaData,
                field: field,
            };
        }
    }

    static private function isFieldDefined(superClass : ClassType, f : Field) : Bool {
        var superSuperClass = superClass.superClass;
        if(superSuperClass != null){
            if(isFieldDefined(superSuperClass.t.get(),f)){
                return true;
            }
        }
        return superClass.fields.get().filter(f2 -> f.name == f2.name).length > 0;
    }
    macro static public function buildEventListener() : Array<Field> {
        var fields : Array<Field> = Context.getBuildFields();

        var stringType = TypeParam.TPType(TPath({name: "String", params: [], pack: []}));

        var updateFunBody = macro {};
        var registerFunBody = macro {};
        var callAfterUpdate = false;
        var callAfterUpdateMacro = macro {};

        for(field in fields){
            switch(field.kind){
                case FFun(fun):
                    for(meta in field.meta){
                        switch(meta.name) {
                            case "afterUpdate":
                                switch(fun.args){
                                    case []:
                                        var fname = field.name;
                                        callAfterUpdate = true;
                                        callAfterUpdateMacro = macro {
                                            ${callAfterUpdateMacro}
                                            this.$fname();
                                        };
                                    default:
                                }
                        }
                    }
                default:
            }
        }

        var initUpdateData = macro {};
        if(callAfterUpdate)initUpdateData = macro var __update_data : Bool = false;

        var execUpdateData = macro {};
        if(callAfterUpdate){
            execUpdateData = macro {
                if(__update_data){
                    ${callAfterUpdateMacro}
                }
            }
        }

        for(field in fields){
            var data = getUpdateableFieldData(field);
            if(data != null){
                var fname = field.name;
                if(!data.field.isStruct){
                    if(data.meta.dependentOnKey){
                        var setUpdate = macro {};
                        if(callAfterUpdate){
                            setUpdate = macro {
                                __update_data = true;
                            };
                        }
                        updateFunBody = macro {
                            ${updateFunBody}
                            updateData.updateMapValIfMatches($v{data.meta.keyName}, $v{data.field.name}, $v{data.field.subfields[0].type}, (key,val) -> {
                                ${setUpdate};
                                this.$fname.set(key, val);
                            });
                            updateData.deleteKeyIfMatches($v{data.meta.keyName}, (key) -> {
                                ${setUpdate};
                                this.$fname.remove(key);
                            });
                        }
                    } else {
                        var setUpdate = macro {};
                        if(callAfterUpdate){
                            setUpdate = macro {
                                if(this.$fname != val)__update_data = true;
                            };
                        }
                        updateFunBody = macro {
                            ${updateFunBody}
                            updateData.updateValIfMatches($v{data.field.name}, $v{data.field.subfields[0].type}, (val) -> {
                                ${setUpdate};
                                this.$fname = val;
                            });
                        }
                    }
                } else {
                    for(subfield in data.field.subfields){
                        var flname = subfield.name;
                        if(data.meta.dependentOnKey){
                            var setUpdate = macro {};
                            if(callAfterUpdate){
                                setUpdate = macro {
                                    if(f.$flname != val)__update_data = true;
                                };
                            }
                            updateFunBody = macro {
                                ${updateFunBody}
                                updateData.updateMapValIfMatches($v{data.meta.keyName}, $v{flname}, $v{subfield.type}, (key,val) -> {
                                    var f = this.$fname.get(key);
                                    if(f == null){
                                        f = ${data.field.initValue};
                                    }
                                    ${setUpdate};
                                    f.$flname = val;
                                    this.$fname.set(key, f);
                                });
                            };
                        } else {
                            var setUpdate = macro {};
                            if(callAfterUpdate){
                                setUpdate = macro {
                                    if(this.$fname.$flname != val)__update_data = true;
                                };
                            }
                            updateFunBody = macro {
                                ${updateFunBody}
                                updateData.updateValIfMatches($v{flname}, $v{subfield.type}, (key,val) -> {
                                    ${setUpdate};
                                    this.$fname.$flname = val;
                                });
                            };
                        }
                    }
                    if(data.meta.dependentOnKey){
                        var setUpdate = macro {};
                        if(callAfterUpdate){
                            setUpdate = macro {
                                if(this.$fname.exists(key))__update_data = true;
                            };
                        }
                        updateFunBody = macro {
                            ${updateFunBody}
                            updateData.deleteKeyIfMatches($v{data.meta.keyName}, (key) -> {
                                ${setUpdate};
                                this.$fname.remove(key);
                            });
                        };
                    }
                }

                if(data.meta.dependentOnKey){
                    var arrayDef = macro $a{[]};
                    arrayDef.pos = data.meta.keyDef;
                    field.kind = FVar(TPath({name: "Map", params:[stringType,TPType(data.field.type)], pack: []}), arrayDef);
                }
            }else{
                switch(field.kind){
                    case FFun(fun):
                        for(meta in field.meta){
                            switch(meta.name) {
                                case "listener":
                                    switch(fun.args){
                                        case [{type:TPath({name:className})}]:
                                            var fname = field.name;
                                            registerFunBody = macro {
                                                ${registerFunBody}
                                                eventEngine.registerFunction($i{className}, this, this.$fname);
                                            };
                                        default:
                                    }
                            }
                        }
                    default:
                }
            };

            for(meta in field.meta){
                switch(meta.name) {
                    case "trace":
                        trace(field);
                    default: continue;
                }
            }
        }
        var classWithNewFields = macro class Tmp{
            public function __update(dataUpdates: List<haxeda.events.UpdateData>) : Void {
                ${initUpdateData}
                for(updateData in dataUpdates){
                    ${updateFunBody}
                }
                ${execUpdateData}
            }
            public function registerToEventEngine(eventEngine: haxeda.EventEngine) : Void {
                eventEngine.registerListener(this);
                ${registerFunBody}
            }
        };
        var superClass = Context.getLocalClass().get().superClass;
        if(superClass != null){
            for(f in classWithNewFields.fields){
                if(isFieldDefined(superClass.t.get(), f))
                {
                    f.access.push(haxe.macro.Expr.Access.AOverride);
                }
            }
        }
        return fields.concat(classWithNewFields.fields);
    }

    macro static public function buildEvent() : Array<Field> {
        var fields : Array<Field> = Context.getBuildFields();

        var funBody = macro {};

        var key = "";
        var keyname = "";

        var metaDataList = new List<{
            name:String,
            fieldname:String,
            isKey:Bool,
            isUpdate:Bool,
            keyPos:haxe.macro.Expr.Position,
            updatePos:haxe.macro.Expr.Position,
            type:String,
            isDelete:Bool,
        }>();
        for(field in fields){
            var metaData = {
                name: field.name,
                fieldname:field.name,
                isKey: false,
                isUpdate: false,
                keyPos: field.pos,
                updatePos: field.pos,
                type: "",
                isDelete: false,
            };
            switch(field.kind){
                case FVar(TPath(path),_):
                    metaData.type = path.name;
                    for(meta in field.meta){
                        switch(meta.name) {
                            case "update" | "key" | "deleteKey":
                                if(meta.name == "update"){
                                    metaData.isUpdate = true;
                                    metaData.updatePos = meta.pos;
                                } else if(meta.name == "key") {
                                    metaData.isKey = true;
                                    metaData.keyPos = meta.pos;
                                } else {
                                    metaData.isKey = true;
                                    metaData.keyPos = meta.pos;
                                    metaData.isDelete = true;
                                }
                                switch(meta.params){
                                    case []:
                                    case [{expr:EConst(CString(name,_))}]:
                                        metaData.name = name;
                                    default: Context.fatalError('@${meta.name} has 1 optional String Parameter', meta.pos);
                                }
                            default: continue;
                        }
                    }
                    if(metaData.isKey && metaData.isUpdate){
                        Context.fatalError("@update and @key are not allowed for same variable. Remove @update", metaData.updatePos);
                    }
                    if(metaData.isDelete && metaData.isUpdate){
                        Context.fatalError("@update and @deleteKey are not allowed for same variable. Remove @update", metaData.updatePos);
                    }
                    if(metaData.isKey && key != ""){
                        Context.fatalError("Multiple @key in same Event are not allowed.", metaData.keyPos);
                    }
                    if(metaData.isKey){
                        key = metaData.name;
                        keyname = metaData.fieldname;
                    }
                    metaDataList.add(metaData);
                default: continue;
            }
        }
        for(metaData in metaDataList){
            var fname = metaData.fieldname;
            if(key == ""){
                funBody = macro {
                    ${funBody}
                    result.add(new haxeda.events.UpdateData("", "", $v{metaData.name}, $v{metaData.type}, this.$fname, $v{metaData.isDelete}));
                };
            } else {
                funBody = macro {
                    ${funBody}
                    result.add(new haxeda.events.UpdateData($v{key}, this.$keyname, $v{metaData.name}, $v{metaData.type}, this.$fname, $v{metaData.isDelete}));
                };
            }
        }
        var classWithNewFields = macro class Tmp{public function __getUpdateData() : List<haxeda.events.UpdateData> {
            var result : List<haxeda.events.UpdateData> = new List<haxeda.events.UpdateData>();
            ${funBody}
            return result;
        }};
        return fields.concat(classWithNewFields.fields);
    }
}
