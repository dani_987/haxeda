## Targets

Haxeda has the following targets and uses therefore the following technologies:

 - All reasons why you use EDA (lose couping, fault tolerance, reduce technial depts)
    + EDA-implemantaion
 - Simplify Errorhandling
    + EventEngineSuperviser
    + EventSaver can save the History
    + Splitting events in User-Events and the Data-Events (that can be created from User-Events)
 - Simplify connection with different systems
    + code should looks same, independed if it runs on one or thousand computers
    + HaxedaCommunicator
    + UserEventOrderer
 - Simple System Configuration
    + Event-State-Maschine (makes it easy to bind events to each other in configuration)
 - Simple Testing
    + haxeda-test
 - Help with anti-Cheating
    + Splitting events in User-Events and the Data-Events
    + Clients can only create User-Events
    + LoginHandler
    + Multiple ClientEngine + 1 ServerEngine
 - Multiuser should be able to have secret values, but also communicate in groups or in public
    + LoginHandler with EncryptionHandler
    + Sending Data-Events can handle the state (Local, Global, Group-By-Name)
    + Adding or removing from Groups
    + EventSaver can implement Encryption
 - Allows to add new ways to communicate, save data or log in
    + EventSaver
    + BaseCommunicator
    + BaseLoginHandler
