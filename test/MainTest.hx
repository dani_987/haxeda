package test;
using buddy.Should;

class MainTest implements buddy.Buddy<[
    unit.EventEngineSupervisorTest,
    new haxeda.test.HaxedaTestExecutor(new test.stories.EventsWereProcessedInCorrectOrder.CorrectOrderTestEngineRegisterer(),new test.stories.EventsWereProcessedInCorrectOrder.CorrectOrderTestUserStoryCreatorManager()),
    new haxeda.test.HaxedaTestExecutor(new test.EmptyEventEngineRegisterer(),new test.UserStoryCreatorManager()),
]>{}
