package unit;

import haxeda.Event;
import haxeda.EventEngineSuperviser;
import haxeda.EventEngine;
import buddy.BuddySuite;

using buddy.Should;

class EventEngineSupervisorTest extends BuddySuite {
    public function new() {
        describe("EventEngineSupervisor", {
            var engine : EventEngine = null;
            var supervisor = new TestEventEngineSupervisor();

            it("should be able to add to EventEngine",{
                engine = new EventEngine(null, [supervisor]);
                supervisor.getEventCount().should.be(0);
            });

            it("should be able to add to receive the Start event",{
                engine.start(RunTilReturns);
                supervisor.getEventCount().should.be(1);
            });
        });
    }
}


class TestEventEngineSupervisor extends EventEngineSuperviser {
    private var countEvents = 0;

    public function new() {}
    override function onEvent(eventName:String, event:Event) {
        countEvents++;
    }

    public function getEventCount() : Int {
        return countEvents;
    }
}
