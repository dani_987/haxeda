package test;

class UserStoryCreatorManager implements haxeda.test.UserStoryCreatorCollectionManager{
    public function new() {}

    public function getAllCreators() : Array<haxeda.test.UserStoryCreator> {
        return [
            new test.stories.EventEnginePassesAllEventsCorrektly(),
            new test.stories.EventEngineTestsDependsCorrectlyOnEachOther(),
            new test.stories.EventEnginePassesAllEventsCorrektlyNotSuccessful(),
            new test.stories.EventEngineUpdatesGlobalDataCorrektly(),
            new test.stories.EventEngineUpdatesKeyValueDataCorrektly(),
            new test.stories.EventEngineDeletesKeyDataCorrektly(),
            new test.stories.EventEngineRepeatsCorrektly(),
            new test.stories.EventEnginePassesEnumsCorrektly(),
            new test.stories.EventEngineCallsAfterUpdate(),
        ];
    }
}
