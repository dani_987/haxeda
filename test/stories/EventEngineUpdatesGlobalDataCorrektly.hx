package test.stories;
import helperclasses.DataUpdateNoListenerFunctionsListener;
import helperclasses.DataUpdateGlobalListerner;
using haxeda.test.UserStory;

class EventEngineUpdatesGlobalDataCorrektly extends haxeda.test.UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        var globalData = true;
        return this.given(
                haxeda.events.ApplicationStarted
            ).when(
                (e) -> { return new test.events.UpdateGlobalData(globalData); }
            ).then(
                test.events.TestDone, (e) -> {
                    return DataUpdateGlobalListerner.instance.checkGlobalData(globalData)
                     && DataUpdateNoListenerFunctionsListener.instance.checkGlobalData(globalData);
                }
            ).isSuccessful();
    }
}
