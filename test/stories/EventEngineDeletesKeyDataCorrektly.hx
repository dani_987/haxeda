package test.stories;
import helperclasses.DataDeleteKeyListerner;
using haxeda.test.UserStory;

class EventEngineDeletesKeyDataCorrektly extends haxeda.test.UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        var key = "Hallo";
        return this.given(
                test.events.TestAddedKeyValue
            ).when(
                (e) -> { return new test.events.DeleteKeyData(key); }
            ).then(
                test.events.TestDone, (e) -> {
                    return ! (DataDeleteKeyListerner.instance.checkKeyExists(key));
                }
            ).isSuccessful();
    }
}
