package test.stories;
using haxeda.test.UserStory;

class EventEngineTestsDependsCorrectlyOnEachOther extends haxeda.test.UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        return this.given(
                test.events.TestDone
            ).when( (e) -> {
                return e;
            }).then(
                test.events.TestDone, (e) -> { return true; }
            ).isSuccessful();
    }
}
