package test.stories;
import helperclasses.DataUpdateNoListenerFunctionsListener;
import helperclasses.DataUpdateGlobalListerner;
using haxeda.test.UserStory;

class EventEngineCallsAfterUpdate extends haxeda.test.UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        return this.given(
                haxeda.events.ApplicationStarted
            ).when(
                (e) -> { return new test.events.UpdateGlobalData(true); }
            ).then(
                test.events.TestDone, (e) -> {
                    return DataUpdateGlobalListerner.instance.checkAfterUpdateCalled();
                }
            ).isSuccessful();
    }
}
