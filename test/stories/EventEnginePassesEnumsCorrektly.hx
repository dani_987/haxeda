package test.stories;
import test.events.TestEnums;
import test.events.TestDone;
import haxeda.EventEngine;
import test.events.TestEnums.SomeEnum;
import haxeda.EventListener;
using haxeda.test.UserStory;

class EventEnginePassesEnumsCorrektly extends haxeda.test.UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        return this.given(
                haxeda.events.ApplicationStarted
            ).when(
                (e) -> { return new test.events.TestEnums(No); }
            ).then(
                test.events.TestDone, (e) -> {
                    return EnumValueListener.instance.get_someEnum() == No;
                }
            ).isSuccessful();
    }
}

class EnumValueListener implements EventListener{
    @update private var someEnum : SomeEnum = Yes;
    private var eventEngine: EventEngine;

    public static var instance:EnumValueListener = new EnumValueListener();

    private function new() {}

    public function get_someEnum() : SomeEnum {return someEnum;}

    public function setupEventEngine(eventEngine: EventEngine) {
        this.eventEngine = eventEngine;
        this.registerToEventEngine(this.eventEngine);
    }

    @listener
    private function onSomeEnumTest(someEnum: TestEnums) : EventProcessingResult {
        this.eventEngine.triggerEvent(new TestDone());
        return Success;
    }
}
