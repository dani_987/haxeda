package test.stories;
import helperclasses.DataUpdateKeyValueListerner;
import helperclasses.OnlyDataUpdateKeyValueListerner;
import helperclasses.DataDeleteKeyListerner;
using haxeda.test.UserStory;

class EventEngineUpdatesKeyValueDataCorrektly extends haxeda.test.UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        var globalData = true;
        var key = "Hallo";
        var someList = new List();
        someList.add("Hallo");
        var someMap : Map <String, Int> = [
            "Hallo" => 1,
            "Hey" => 5,
        ];
        return this.given(
                haxeda.events.ApplicationStarted
            ).when(
                (e) -> {
                    return new test.events.UpdateKeyValueData(key, globalData, someList, someMap);
                }
            ).then(
                test.events.TestAddedKeyValue, (e) -> {
                    return DataUpdateKeyValueListerner.instance.checkKeyValueData(key, globalData)
                    && !DataUpdateKeyValueListerner.instance.checkKeyValueData('${key}_', globalData)
                    && DataUpdateKeyValueListerner.instance.checkListData(key, someList)
                    && DataUpdateKeyValueListerner.instance.checkMapData(key, someMap)
                    && DataDeleteKeyListerner.instance.checkKeyExists(key)
                    && OnlyDataUpdateKeyValueListerner.instance.checkKeyValueData(key, globalData);
                }
            ).isSuccessful();
    }
}
