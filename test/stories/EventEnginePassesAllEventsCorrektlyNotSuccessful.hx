package test.stories;
using haxeda.test.UserStory;

class EventEnginePassesAllEventsCorrektlyNotSuccessful extends haxeda.test.UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        return this.given(
                haxeda.events.ApplicationStarted
            ).when(
                (e) -> { return new test.events.TestStart(); }
            ).then(
                test.events.TestDone, (e) -> {
                    return false;
                }
            ).withExepctedError();
    }
}
