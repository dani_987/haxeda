package test.stories;
import test.events.TestDone;
import test.events.DoSomeWork;
using haxeda.test.UserStory;

class EventEngineRepeatsCorrektly extends haxeda.test.UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        return this.given(
                haxeda.events.ApplicationStarted
            ).when(
                (e) -> { return new DoSomeWork(); }
            ).then(
                TestDone, (e) -> {
                    return true;
                }
            ).isSuccessful();
    }
}
