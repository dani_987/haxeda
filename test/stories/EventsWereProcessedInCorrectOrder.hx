package test.stories;

import test.EmptyEventEngineRegisterer.TestPasses;
import haxeda.test.UserStory;
import haxeda.events.ApplicationStarted;
import haxeda.test.UserStoryCreator;
import test.events.TestStart;
import test.events.TestDone;
import haxeda.EventEngine;
import haxeda.EventEngineRegisterer;
import haxeda.EventListener;
import haxeda.Event;

class UpdateSomeInt implements Event {
    @update private var someInt:Int;
    public function new(newSomeIntValue: Int) {
        this.someInt = newSomeIntValue;
    }
}

class SomeIntValueListener implements EventListener {
    private static var instance = new SomeIntValueListener();
    private function new() {}
    @update private var someInt : Int = 0;

    public static function setupEventEngine(eventEngine: EventEngine) {
        instance.registerToEventEngine(eventEngine);
    }

    public static function checkSomeInt(someIntIs: Int) : Bool {
        return instance.someInt == someIntIs;
    }
}


class CorrectOrderTestEngineRegisterer implements EventEngineRegisterer {
    public function new() {}

    public function registerToEventEngine(eventEngineToRegister:EventEngine):Bool {
        SomeIntValueListener.setupEventEngine(eventEngineToRegister);
        new TestPasses(eventEngineToRegister);
        return true;
    }
}

class CorrectOrderTestUserStoryCreatorManager implements haxeda.test.UserStoryCreatorCollectionManager {
    public function new() {}

    public function getAllCreators() : Array<haxeda.test.UserStoryCreator> {
        return [ new EventsWereProcessedInCorrectOrder() ];
    }
}

class EventsWereProcessedInCorrectOrder extends UserStoryCreator {
    public function new() {}

    function createStory():UserStory {
        return this.given(
            ApplicationStarted
        ).whenAll((_)->{return [
            new UpdateSomeInt(1),
            new TestStart(),
        ];}).andWhen(TestDone, (_) -> {
            if(!SomeIntValueListener.checkSomeInt(1))return null;
            return [
                new UpdateSomeInt(2),
                new TestStart(),
            ];
        }).andWhen(TestDone, (_) -> {
            if(!SomeIntValueListener.checkSomeInt(2))return null;
            return [
                new UpdateSomeInt(3),
                new TestStart(),
            ];
        }).then(TestDone, (_)->{
            return SomeIntValueListener.checkSomeInt(3);
        }).isSuccessful();
    }
}
