package test;

import helperclasses.DataUpdateNoListenerFunctionsListener;
import test.stories.EventEnginePassesEnumsCorrektly.EnumValueListener;
import helperclasses.DataDeleteKeyListerner;
import helperclasses.DataUpdateGlobalListerner;
import helperclasses.DataUpdateKeyValueListerner;
import helperclasses.OnlyDataUpdateKeyValueListerner;
import helperclasses.RepeatWorkListener;
import haxeda.EventListener;
import haxeda.EventEngine;
import haxeda.EventEngineRegisterer;

import test.events.*;

class TestPasses implements EventListener {
	private var eventEngine: EventEngine;

	public function new(eventEngine: EventEngine) {
		this.eventEngine = eventEngine;
		this.registerToEventEngine(eventEngine);
	}

	@listener
	public function onEvent(eventOccured:TestStart):EventProcessingResult {
		eventEngine.triggerEvent(new TestDone());
		return Success;
	}
}
class EmptyEventEngineRegisterer implements EventEngineRegisterer {
    public function new(){}

	public function registerToEventEngine(eventEngineToRegister:EventEngine):Bool {
		#if test
			new TestPasses(eventEngineToRegister);

			DataUpdateGlobalListerner.instance.setupEventEngine(eventEngineToRegister);
			DataUpdateNoListenerFunctionsListener.instance.setupEventEngine(eventEngineToRegister);
			DataUpdateKeyValueListerner.instance.setupEventEngine(eventEngineToRegister);
			OnlyDataUpdateKeyValueListerner.instance.setupEventEngine(eventEngineToRegister);
			DataDeleteKeyListerner.instance.setupEventEngine(eventEngineToRegister);
			RepeatWorkListener.instance.setupEventEngine(eventEngineToRegister);
			EnumValueListener.instance.setupEventEngine(eventEngineToRegister);

			eventEngineToRegister.registerListener(OnlyDataUpdateKeyValueListerner.instance);
		#end
		return true;
	}
}
