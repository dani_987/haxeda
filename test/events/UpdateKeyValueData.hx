package test.events;

enum TableEnum {
    EnumTestOk;
    EnumTestNotOk;
}

class UpdateKeyValueData implements haxeda.Event {
    @update var value : Bool;
    @update var tableEnum: TableEnum;
    @update var someList: List<String>;
    @update var someMap: Map<String,Int>;
    @key var key : String;

    public function new(key: String, value : Bool, someList: List<String>, someMap: Map<String,Int>){
        this.key = key;
        this.value = value;
        this.tableEnum = EnumTestOk;
        this.someList = someList;
        this.someMap = someMap;
    }
}
