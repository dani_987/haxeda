package test.events;


enum SomeEnum {
    Yes;
    No;
}
class TestEnums implements haxeda.Event {
    @update private var someEnum : SomeEnum;
    public function new(someEnum : SomeEnum) {
        this.someEnum = someEnum;
    }
}
