package test.events;

class DeleteKeyData implements haxeda.Event {
    @deleteKey var key : String;

    public function new(key: String){
        this.key = key;
    }
}
