package test.events;

class UpdateGlobalData implements haxeda.Event {
    @update var valueSend : Bool;

    public function new(valueSend : Bool){
        this.valueSend = valueSend;
    }
}
