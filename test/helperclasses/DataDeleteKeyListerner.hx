package helperclasses;

import test.events.TestDone;
import test.events.DeleteKeyData;
import haxeda.EventEngine;
import haxeda.EventListener;


class DataDeleteKeyListerner implements EventListener{
	@keyTable("key") var value : Bool = false;

	private var eventEngine: EventEngine;
	public function setupEventEngine(eventEngine: EventEngine){
		this.eventEngine = eventEngine;
		this.registerToEventEngine(eventEngine);
	}

	public static final instance = new DataDeleteKeyListerner();

	private function new() {}

	@listener
	public function onUpdateKeyValueData(eventOccured:DeleteKeyData) : EventProcessingResult {
		this.eventEngine.triggerEvent(new TestDone());
		return Success;
	}

	public function checkKeyExists(key: String) : Bool {
		return this.value.exists(key);
	}
}
