package helperclasses;

import test.events.UpdateKeyValueData;
import test.events.TestAddedKeyValue;
import haxeda.EventEngine;
import haxeda.events.UpdateData;
import haxeda.EventListener;


class DataUpdateKeyValueListerner implements EventListener{
	@keyTable("key") var value : Bool = false;
	@keyTable("key") var tableEnum : TableEnum = EnumTestNotOk;
	@keyTable("key") var someList : List<String>;
    @keyTable("key") var someMap: Map<String,Int>;

	private var eventEngine: EventEngine;
	public function setupEventEngine(eventEngine: EventEngine){
		this.eventEngine = eventEngine;
		this.registerToEventEngine(eventEngine);
	}

	public static final instance = new DataUpdateKeyValueListerner();

	private function new() {}

	@listener
	public function onUpdateKeyValueData(eventOccured:UpdateKeyValueData) : EventProcessingResult {
		this.eventEngine.triggerEvent(new TestAddedKeyValue());
		return Success;
	}

	public function checkKeyValueData(key: String, toCheck: Bool) : Bool {
		return (this.value.get(key) == toCheck) && (this.tableEnum.get(key) == EnumTestOk);
	}

	public function checkListData(key: String, toCheck: List<String>) : Bool {
		var listToCompare = this.someList.get(key);
		return '$listToCompare' == '$toCheck';
	}

	public function checkMapData(key: String, toCheck: Map<String,Int>) : Bool {
		var mapToCompare = this.someMap.get(key);
		return '$mapToCompare' == '$toCheck';
	}
}
