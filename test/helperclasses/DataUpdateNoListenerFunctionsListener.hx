package helperclasses;

import haxeda.EventEngine;
import haxeda.events.UpdateData;
import haxeda.EventListener;


class DataUpdateNoListenerFunctionsListener implements EventListener{
	@update var valueSend : Bool = false;

	public function setupEventEngine(eventEngine: EventEngine){
		this.registerToEventEngine(eventEngine);
	}
	public static final instance = new DataUpdateNoListenerFunctionsListener();

	private function new() {}

	public function checkGlobalData(toCheck: Bool) : Bool { return (this.valueSend == toCheck);}
}
