package helperclasses;

import test.events.TestDone;
import test.events.DoSomeWork;
import haxeda.EventEngine;
import haxeda.EventListener;


class RepeatWorkListener implements EventListener{
	private var eventEngine: EventEngine;
	public function setupEventEngine(eventEngine: EventEngine){
		this.eventEngine = eventEngine;
		this.registerToEventEngine(eventEngine);
	}

	public static final instance = new RepeatWorkListener();

	private static var onWorkCalled : Int = 0;

	private function new() {}

	@listener
	public function onWork(eventOccured:DoSomeWork) : EventProcessingResult {
		onWorkCalled++;
		if(onWorkCalled <= 1)return StopProcessing(75);
		this.eventEngine.triggerEvent(new TestDone());
		return Success;
	}

}
