package helperclasses;

import test.events.UpdateGlobalData;
import test.events.TestDone;
import haxeda.EventEngine;
import haxeda.events.UpdateData;
import haxeda.EventListener;


class DataUpdateGlobalListerner implements EventListener{
	@update var valueSend : Bool = false;
	private var afterUpdateCalled : Bool = false;

	private var eventEngine: EventEngine;
	public function setupEventEngine(eventEngine: EventEngine){
		this.eventEngine = eventEngine;
		this.registerToEventEngine(eventEngine);
	}

	public static final instance = new DataUpdateGlobalListerner();

	private function new() {}

	@listener
	public function onUpdateGlobalData(updateData:UpdateGlobalData) : EventProcessingResult {
		this.eventEngine.triggerEvent(new TestDone());
		return Success;
	}

	public function checkGlobalData(toCheck: Bool) : Bool { return (this.valueSend == toCheck);}

	public function checkAfterUpdateCalled() : Bool { return afterUpdateCalled;}

	@afterUpdate
	private function afterUpdateGlobalData() {
		afterUpdateCalled = true;
	}
}
