package helperclasses;

import haxeda.EventEngine;
import haxeda.EventListener;

typedef SomeStruct = {name: String, value: Bool};
typedef SomeOtherStruct = { > SomeStruct, someAdditionalValue: String };
class OnlyDataUpdateKeyValueListerner implements EventListener{
	@keyTable("key") var value : Bool = false;
	@keyTable("key") var foo : {name: String, value: Bool};
	@keyTable("key") var foo2 : SomeStruct;
	@keyTable("key") var foo3 : { > SomeStruct, someAdditionalValue: String };
	@keyTable("key") var foo4 : SomeOtherStruct;

	public static final instance = new OnlyDataUpdateKeyValueListerner();

	private function new() {}

	private var eventEngine: EventEngine;
	public function setupEventEngine(eventEngine: EventEngine){
		this.eventEngine = eventEngine;
		this.registerToEventEngine(eventEngine);
	}

	public function checkKeyValueData(key: String, toCheck: Bool) : Bool {
		return (this.value.get(key) == this.foo.get(key).value)
				&& (this.foo.get(key).value == this.foo2.get(key).value)
				&& (this.foo.get(key).value == this.foo3.get(key).value)
				&& (this.foo.get(key).value == this.foo4.get(key).value)
				&& (this.value.get(key) == toCheck);
	}
}
